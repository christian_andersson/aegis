var { ToggleButton } = require('sdk/ui/button/toggle');
var panels = require("sdk/panel");
var self = require("sdk/self");
var tabs = require("sdk/tabs");

var activeTabURL = tabs.activeTab.url;

tabs.on("activate", tab => {
  activeTabURL = tab.url;
});

tabs.on("ready", tab => {
  if (tab == tabs.activeTab) {
    activeTabURL = tab.url;
  }
});

var button = ToggleButton({
  id: "my-button",
  label: "my button",
  icon: {
    "16": "./img/icon-16.png",
    "32": "./img/icon-32.png",
    "64": "./img/icon-64.png"
  },
  onChange: handleChange
});

function handleChange(state) {
  if (state.checked) {
	var panel = panels.Panel({
	  width: 640,
	  height: 400,
	  contentURL: self.data.url("index.html"),
	  contentScriptFile: self.data.url("panel.js"),
	  onHide: handleHide
	});
    panel.show({
      position: button
    });
	panel.on("show", function() {
	  panel.port.emit("show", activeTabURL);
	});
  }
}

function handleHide() {
  button.state('window', {checked: false});
}
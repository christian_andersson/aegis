package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetTimeServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	String chk1, chk2, chk3, chk4, chk5, chk6, chk7, chk8, chk9, chk10, chk11, chk12;
	int userrating = -1;

	public void doPost (HttpServletRequest request,HttpServletResponse response) 
			throws ServletException, IOException 
	{
	    StringBuilder sb = new StringBuilder();
	    BufferedReader reader = request.getReader();
	    try {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line);
	        }
	    } finally {
	        reader.close();
	    }
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Access-Control-Allow-Origin", "*");
		PrintWriter out = response.getWriter();
		newtest.DbQueries dbq = new newtest.DbQueries();
		
		String postvar = sb.toString();
		String[] postarr = new String[20];
		postarr = postvar.split("&");
		
		String site = postarr[0].split("=")[1];
		String usercomment = null;
		try {
			userrating = (int) Double.parseDouble(postarr[1].split("=")[1]);
			usercomment = postarr[2].split("=")[1];
			chk1 = postarr[3].split("=")[1];
			chk2 = postarr[4].split("=")[1];
			chk3 = postarr[5].split("=")[1];
			chk4 = postarr[6].split("=")[1];
			chk5 = postarr[7].split("=")[1];
			chk6 = postarr[8].split("=")[1];
			chk7 = postarr[9].split("=")[1];
			chk8 = postarr[10].split("=")[1];
			chk9 = postarr[11].split("=")[1];
			chk10 = postarr[12].split("=")[1];
			chk11 = postarr[13].split("=")[1];
			chk12 = postarr[14].split("=")[1];
		} catch (ArrayIndexOutOfBoundsException e) {
			//do nothing
		}		
		
		System.out.println("Site: " + site);
		System.out.println("Rating: " + userrating);
		System.out.println("Comment: " + usercomment);
		System.out.println("Checkbox 1: " + chk1);
		System.out.println("Checkbox 2: " + chk2);
		System.out.println("Checkbox 3: " + chk3);
		System.out.println("Checkbox 4: " + chk4);
		System.out.println("Checkbox 5: " + chk5);
		System.out.println("Checkbox 6: " + chk6);
		System.out.println("Checkbox 7: " + chk7);
		System.out.println("Checkbox 8: " + chk8);
		System.out.println("Checkbox 9: " + chk9);
		System.out.println("Checkbox 10: " + chk10);
		System.out.println("Checkbox 11: " + chk11);
		System.out.println("Checkbox 12: " + chk12);
		
		System.out.println("");
		site = trimUrl(site);
		out.print(dbq.getSiteScore(site));
	}
	
	private String trimUrl(String url) {
		String result, host;
		try {
			result = java.net.URLDecoder.decode(url, "UTF-8");
			URL address;
			try {
				address = new URL(result);
				host = address.getHost();
				int idx = host.indexOf(".");
				host = host.substring(idx+1);
			} catch (MalformedURLException e) {
				host = "error";
			}
			return host;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
}